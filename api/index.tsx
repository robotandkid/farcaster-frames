import { Button, Frog, TextInput } from "frog";
// import { neynar } from 'frog/hubs'
import { handle } from "frog/vercel";

// Uncomment to use Edge Runtime.
// export const config = {
//   runtime: 'edge',
// }

export const app = new Frog({
  assetsPath: "/",
  basePath: "/api",
  // Supply a Hub to enable frame verification.
  // hub: neynar({ apiKey: 'NEYNAR_FROG_FM' })
});

app.frame("/", (c) => {
  const { buttonValue, inputText, status } = c;
  const fruit = inputText || buttonValue;
  return c.res({
    image: (
      <div
        style={{
          alignItems: "center",
          background:
            status === "response"
              ? "linear-gradient(to right, #432889, #17101F)"
              : "black",
          backgroundSize: "100% 100%",
          display: "flex",
          flexDirection: "column",
          flexWrap: "nowrap",
          height: "100%",
          justifyContent: "center",
          textAlign: "center",
          width: "100%",
        }}
      >
        <div
          style={{
            color: "white",
            fontSize: 60,
            fontStyle: "normal",
            letterSpacing: "-0.025em",
            lineHeight: 1.4,
            marginTop: 30,
            padding: "0 120px",
            whiteSpace: "pre-wrap",
          }}
        >
          {status === "response"
            ? `Nice choice.${fruit ? ` ${fruit.toUpperCase()}!!` : ""}`
            : "Welcome!"}
        </div>
      </div>
    ),
    intents: [
      <TextInput placeholder="Enter custom fruit..." />,
      <Button value="apples">Apples</Button>,
      <Button value="oranges">Oranges</Button>,
      <Button value="bananas">Bananas</Button>,
      status === "response" && <Button.Reset>Reset</Button.Reset>,
    ],
  });
});

app.frame("/kung-fu", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/kung-fu.gif",
    imageAspectRatio: "1:1",
    intents: [
      <Button.Link href="https://objkt.com/tokens/KT1HYRWr9vHFRyp7sKYar1i2Tq9FuDhWiNwD/22">
        Mint on Objkt!
      </Button.Link>,
    ],
  });
});

app.frame("/vader", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/vader-lg.gif",
    imageAspectRatio: "1:1",
    intents: [],
  });
});

app.frame("/nft-comic/sometimes-you-just-need-to-rest", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/nft-comic/sometimes-you-just-need-to-rest.gif",
    imageAspectRatio: "1:1",
    intents: [
      <Button.Link href="https://objkt.com/tokens/KT1KTG9dyqF5KZzA5dBa62LYXeRmPcPiWowN/21">
        View on Objkt
      </Button.Link>,
    ],
  });
});

app.frame("/noise-canvas/family-time", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/noise-canvas/family-time.gif",
    imageAspectRatio: "1:1",
    intents: [
      <Button.Link href="https://objkt.com/tokens/KT1HYRWr9vHFRyp7sKYar1i2Tq9FuDhWiNwD/9">
        View on Objkt
      </Button.Link>,
    ],
  });
});

app.frame("/noise-canvas/explaining-bitcoin", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/noise-canvas/explaining-bitcoin.gif",
    imageAspectRatio: "1:1",
    intents: [
      <Button.Link href="https://objkt.com/tokens/KT1HYRWr9vHFRyp7sKYar1i2Tq9FuDhWiNwD/7">
        View on Objkt
      </Button.Link>,
    ],
  });
});

app.frame("/druj-report/04", (c) => {
  // const { buttonValue, inputText, status } = c
  return c.res({
    imageOptions: {
      width: 512,
      height: 512,
    },
    image: "/druj-report/04-sm.gif",
    imageAspectRatio: "1:1",
    intents: [],
  });
});

export const GET = handle(app);
export const POST = handle(app);
